const validateString = (input) => {
	if (/^[a-zA-Z_]+$/.test(input)) {
		return true;
	}
	return 'Invalid value! Must be a string, that contains only letters [a-zA-Z].';
};

const validateNumber = (input) => {
	if (/^[1-9][0-9]*$/.test(input)) {
		return true;
	}
	return 'Invalid value! Must be a number greater than 0.';
};

export default (plop) => {
	const addSubComponentsPrompts = (answers) => {
		const count = answers.subComponentsCount;
		const questions = [];

		for (let i = 0; i < count; i++) {
			questions.push(
				{
					type: 'input',
					name: `subComponents[${i}]`,
					message: `Type a name for a subComponent #${i + 1}?`,
					validate: validateString,
				},
			);
		}

		return questions;
	};

	plop.setGenerator('component', {
		prompts: (inquirer) => inquirer
			.prompt([{
				type: 'input',
				name: 'name',
				message: 'Type a component name please.',
				validate: validateString,
			}, {
				type: 'confirm',
				name: 'hasSubComponents',
				message: 'Should component include subComponents?',
			}, {
				type: 'number',
				name: 'subComponentsCount',
				message: 'How many subComponets it should contain?',
				validate: validateNumber,
				when: (answers) => answers.hasSubComponents,
			}])
			.then((answers) => {
				const { subComponentsCount } = answers;
				if (subComponentsCount) {
					const questions = addSubComponentsPrompts(answers);
					return inquirer
						.prompt(questions)
						.then((subComponentAnswers) => ({ ...answers, ...subComponentAnswers }));
				}
				return answers;
			}),
		actions: (data) => {
			const actions = [
				{
					type: 'add',
					path: 'src/components/{{pascalCase name}}/index.ts',
					templateFile: 'plop-templates/component/index.hbs',
				},
				{
					type: 'add',
					path: 'src/components/{{pascalCase name}}/{{pascalCase name}}.tsx',
					templateFile: 'plop-templates/component/component.hbs',
				},
				{
					type: 'add',
					path: 'src/components/{{pascalCase name}}/{{pascalCase name}}.stories.tsx',
					templateFile: 'plop-templates/component/component.stories.hbs',
				},
			];

			if (data.hasSubComponents && Array.isArray(data.subComponents)) {
				data.subComponents.forEach((subComponent) => {
					actions.push({
						type: 'add',
						data: { ...data, subComponent },
						path: 'src/components/{{pascalCase name}}/components/{{pascalCase subComponent}}/index.ts',
						templateFile: 'plop-templates/subComponent/index.hbs',
					},
					{
						type: 'add',
						data: { ...data, subComponent },
						path: 'src/components/{{pascalCase name}}/components/{{pascalCase subComponent}}/{{pascalCase subComponent}}.tsx',
						templateFile: 'plop-templates/subComponent/subComponent.hbs',
					});
				})
			}

			return actions
		},
	});

	plop.setGenerator('reducer', {
		prompts: [{
			type: 'input',
			name: 'name',
			message: 'Type a reducer name please.',
		}],
		actions: [{
			type: 'add',
			path: 'src/reducers/{{camelCase name}}/index.ts',
			templateFile: 'plop-templates/reducer/index.hbs',
		}, {
			type: 'add',
			path: 'src/reducers/{{camelCase name}}/reducer.ts',
			templateFile: 'plop-templates/reducer/reducer.hbs',
		}],
	});

	plop.setGenerator('action', {
		prompts: (inquirer) => inquirer
			.prompt([{
				type: 'input',
				name: 'name',
				message: 'Type an action name please.',
				validate: validateString,
			}, {
				type: 'confirm',
				name: 'hasActions',
				message: 'Should it already include actions?',
			}, {
				type: 'number',
				name: 'actionsCount',
				message: 'How many actions it should contain?',
				validate: validateNumber,
				when: (answers) => answers.hasActions,
			}])
			.then((answers) => {
				const { actionsCount } = answers;
				if (actionsCount) {
					const questions = [];

					for (let i = 0; i < actionsCount; i++) {
						questions.push(
							{
								type: 'input',
								name: `actions.[${i}].name`,
								message: `Type a 'name' of an action #${i + 1}.`,
								validate: validateString,
							},
							{
								type: 'input',
								name: `actions.[${i}].type`,
								message: `Type a 'type' of an action #${i + 1}.`,
								validate: validateString,
							},
						);
					}

					return inquirer
						.prompt(questions)
						.then((actionsAnswers) => ({ ...answers, ...actionsAnswers }));
				}
				return answers;
			}),
		actions: [{
			type: 'add',
			path: 'src/actions/{{camelCase name}}/index.ts',
			templateFile: 'plop-templates/action/index.hbs',
		}, {
			type: 'add',
			path: 'src/actions/{{camelCase name}}/actions.ts',
			templateFile: 'plop-templates/action/action.hbs',
		}, {
			type: 'modify',
			path: 'src/helpers/constants.ts',
			pattern: /(-- append constants --)/gi,
			template: '$1{{#each actions}}{{#with this}}\r\nexport const {{type}}: \'{{type}}\' = \'{{type}}\'{{/with}}{{/each}}',
		}],
	})
}
