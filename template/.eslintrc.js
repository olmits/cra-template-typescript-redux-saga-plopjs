module.exports = {
  env: {
    es2021: true
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'plugin:storybook/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['react', '@typescript-eslint'],
  rules: {
    'no-tabs': [2, { allowIndentationTabs: true }],
    'indent': 0,
    "react/jsx-indent-props": [2, 'tab'],
    'react/jsx-indent': [2, 'tab'],
    'no-plusplus': [2, { allowForLoopAfterthoughts: true }],
    'max-len': [2, { 'code': 120 }],
    'react/jsx-filename-extension': [1, {
      extensions: ['.ts', '.tsx']
    }],
    'import/extensions': [2, 'ignorePackages', {
      'ts': 'never',
      'tsx': 'never'
    }],
    'import/no-extraneous-dependencies': [
      2,
      {
        devDependencies: [
          '.storybook/**',
          'stories/**',
          '**/*.stories.tsx'
        ]
      }
    ],
    'react/function-component-definition': [2, {
      'namedComponents': 'arrow-function',
      'unnamedComponents': 'arrow-function'
    }],
    'react/jsx-props-no-spreading': 0,
    'no-restricted-exports': 0,
  },
  settings: {
    'import/resolver': {
      'node': {
        'extensions': ['.ts', '.tsx']
      },
      'webpack': {}
    },
    'import/extensions': ['.ts', '.tsx'],
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx']
    },
    'import/core-modules': [],
    'import/ignore': ['node_modules', '\\.(coffee|scss|css|less|hbs|svg|json)$']
  }
};