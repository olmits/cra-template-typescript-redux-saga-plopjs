import { addDecorator } from '@storybook/react';
import { ThemeProvider as Emotion10ThemeProvider } from 'emotion-theming';
import { ThemeProvider, StyledEngineProvider, createTheme } from '@mui/material/styles';
import { MemoryRouter } from "react-router-dom";

const defaultTheme = createTheme();

addDecorator(storyFn => (
  <MemoryRouter>
    <Emotion10ThemeProvider theme={defaultTheme}>
      <ThemeProvider theme={defaultTheme}>
        <StyledEngineProvider injectFirst>
          {storyFn()}
        </StyledEngineProvider>
      </ThemeProvider>
    </Emotion10ThemeProvider>
  </MemoryRouter>
));


export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  layout: 'centered',
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  docs: {
    inlineStories: false,
    iframeHeight: '700px',
  },
}