# no README - no LOVE

## Required deps, that might not be installed by CRA

``` bash
npm i --save-dev @storybook/addon-actions@6.4.22 @storybook/addon-essentials@6.4.22 @storybook/addon-interactions@6.4.22 @storybook/addon-links@6.4.22 @storybook/builder-webpack5@6.4.22 @storybook/manager-webpack5@6.4.22 @storybook/node-logger@6.4.22 @storybook/preset-create-react-app@4.1.0 @storybook/react@6.4.22 @storybook/testing-library@0.0.11 eslint-plugin-storybook@0.5.11 --legacy-peer-deps
```
