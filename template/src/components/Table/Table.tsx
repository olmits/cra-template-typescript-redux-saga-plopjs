import React from 'react';
import {
	TableContainer as MuiTableContainer,
	Table as MuiTable,
} from '@mui/material';
import { TableProps, DefaultObjectType } from './Table.interface';
// import TableHead from './components/TableHead';
import TableBody from './components/TableBody';
import TableRow from './components/TableRow';
import TableCell from './components/TableCell';

const Table = <ObjectType extends DefaultObjectType, >({
	data,
	columns,
}: TableProps<ObjectType>) => (
	<MuiTableContainer component="div">
		<MuiTable component="div">
			<TableBody>
				{data.map((item) => (
					<TableRow>
						{columns.map(({ field }) => (
							<TableCell field={field} data={item} />
						))}
					</TableRow>
				))}
			</TableBody>
		</MuiTable>
	</MuiTableContainer>
);

export default Table;
