import React from 'react';
import { Button, Link, TextField } from '@mui/material';
import { Story, Meta } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { ColumnType, DefaultObjectType, TableProps } from './Table.interface';
import Table from '.';

interface DataItem extends DefaultObjectType {
  id: string;
  timestamp: number;
  name: string;
  bonus: number;
  balance: number;
  totalBonus: number;
  totalBalance: number;
}

const defaultData: DataItem[] = Array.from({ length: 10 }, (_, i) => ({
	id: `${i}`,
	timestamp: 1650378553284,
	name: 'Starlight Princess',
	bonus: i,
	balance: 0.16,
	totalBonus: 0,
	totalBalance: 623.35,
}));

const columns: ColumnType<DataItem>[] = [
	{
		field: 'timestamp',
		headerName: 'TIMESTAMP',
		sortable: true,
		flex: 1,
		align: 'center',
		headerAlign: 'center',
		renderHeader: ({ value }) => <TextField disabled margin="dense" defaultValue={value} variant="outlined" />,
		valueFormatter: ({ value }) => typeof value === 'number' && new Date(value).toLocaleDateString(),
		mobileConfig: {
			hide: true,
		},
	},
	{
		field: 'name',
		headerName: 'GAME',
		sortable: true,
		flex: 2,
	},
	{
		field: 'totalBonus',
		headerName: 'TOTAL BONUS',
		sortable: true,
		flex: 1,
	},
	{
		field: 'totalBalance',
		headerName: 'TOTAL BALANCE',
		sortable: true,
		flex: 1,
		valueFormatter: ({ value }) => (
			typeof value === 'number'
      && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value)),
	},
	{
		field: 'bonus',
		headerName: 'BONUS',
		sortable: true,
		flex: 1,
		renderCell: ({ value }) => (
			<Link component="a" href="/">
				{value}
			</Link>
		),
	},
	{
		field: 'balance',
		headerName: 'BALANCE',
		sortable: true,
		flex: 1,
		renderCell: ({ value }) => <Button>{value}</Button>,
	},
];

export default {
	title: 'Components/Table',
	component: Table,
	args: {
		data: defaultData,
		columns,
		sortModel: { field: 'balance', sort: 'desc' },
		onSortModelChange: action('onSortModelChange'),
	},
} as Meta;

/**
 * Template for Table story
 */
const Template: Story<TableProps<DataItem>> = (args) => (
	<div style={{ width: '90vw' }}>
		<Table {...args} />
	</div>
);

/**
 * Stories
 */
export const Default = Template.bind({});

/**
 * With custom rowHeight
 */
export const CustomHeights = Template.bind({});
CustomHeights.args = {
	rowHeight: 40,
	headerHeight: 60,
};

/**
 * With custom col width
 */
export const CustomColWidth = Template.bind({});
CustomColWidth.args = {
	columns: [
		{
			field: 'timestamp',
			headerName: 'TIMESTAMP',
			sortable: true,
			flex: 1,
			minWidth: 150,
			maxWidth: 200,
			valueFormatter: ({ value }) => typeof value === 'number' && new Date(value).toLocaleDateString(),
			mobileConfig: {
				hide: true,
			},
		},
		{
			field: 'name',
			headerName: 'GAME',
			sortable: true,
			flex: 2,
			minWidth: 100,
			maxWidth: 300,
			mobileConfig: {
				rowIndex: 0,
				renderCell: ({ value, data }) => (
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							justifyContent: 'space-around',
							lineHeight: 'initial',
							height: '100%',
						}}
					>
						<div
							style={{
								fontSize: '16px',
								fontWeight: 700,
								color: '#000',
								overflow: 'hidden',
								textOverflow: 'ellipsis',
							}}
						>
							{value}
						</div>
						<div style={{ fontSize: '12px' }}>
							{data
								&& typeof data.timestamp === 'number'
								&& new Date(data.timestamp).toLocaleDateString()}
						</div>
					</div>
				),
			},
		},
		{
			field: 'totalBonus',
			headerName: 'TOTAL BONUS',
			sortable: true,
			valueFormatter: ({ value }) => typeof value === 'number'
        && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value),
			flex: 1,
			mobileConfig: {
				rowIndex: 0,
			},
		},
		{
			field: 'bonus',
			headerName: 'BONUS',
			sortable: true,
			valueFormatter: ({ value }) => typeof value === 'number'
        && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value),
			flex: 1,
			mobileConfig: {
				rowIndex: 1,
				flex: 2,
				renderCell: ({ value }) => (
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							justifyContent: 'space-around',
							lineHeight: 'initial',
							height: '100%',
						}}
					>
						<div style={{
							fontSize: '10px', color: '#707070', overflow: 'hidden', textOverflow: 'ellipsis',
						}}
						>
							BONUS
						</div>
						<div style={{ fontSize: '12px' }}>
							{typeof value === 'number'
                && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value)}
						</div>
					</div>
				),
			},
		},
		{
			field: 'balance',
			headerName: 'BALANCE',
			sortable: true,
			valueFormatter: ({ value }) => typeof value === 'number'
        && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value),
			flex: 1,
			mobileConfig: {
				rowIndex: 1,
				flex: 2,
				renderCell: ({ value }) => (
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							justifyContent: 'space-around',
							lineHeight: 'initial',
							height: '100%',
						}}
					>
						<div style={{
							fontSize: '10px', color: '#707070', overflow: 'hidden', textOverflow: 'ellipsis',
						}}
						>
							BALANCE
						</div>
						<div style={{ fontSize: '12px' }}>
							{typeof value === 'number'
                && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value)}
						</div>
					</div>
				),
			},
		},
		{
			field: 'totalBalance',
			headerName: 'TOTAL BALANCE',
			sortable: true,
			valueFormatter: ({ value }) => typeof value === 'number'
        && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value),
			flex: 1,
			mobileConfig: {
				rowIndex: 1,
				flex: 3,
				renderCell: ({ value }) => (
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							justifyContent: 'space-around',
							lineHeight: 'initial',
							height: '100%',
						}}
					>
						<div style={{
							fontSize: '10px', color: '#707070', overflow: 'hidden', textOverflow: 'ellipsis',
						}}
						>
							TOTAL BALANCE
						</div>
						<div style={{ fontSize: '12px' }}>
							{typeof value === 'number'
                && new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value)}
						</div>
					</div>
				),
			},
		},
	],
};

/**
 * With pagination
 */
export const WithPagination = Template.bind({});
WithPagination.args = {
	count: 100,
	page: 2,
	pageSize: 10,
	onPageChange: action('onPageChange'),
};
