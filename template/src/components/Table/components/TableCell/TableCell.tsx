/* eslint-disable react/require-default-props */
import React, { ReactElement } from 'react';
import {
	TableCell as MuiTableCell,
	TableCellProps as MuiTableCellProps,
} from '@mui/material';
import { styled } from '@mui/material/styles';

import { DefaultObjectType, CellValue, RenderCellParams } from '../../Table.interface';

interface StyledTableCellProps extends MuiTableCellProps {
  flex?: number | string;
  width?: number;
  height?: number;
  minWidth?: number;
  maxWidth?: number;
}

const StyledTableCell = styled(({
	width = 100,
	minWidth = 50,
	maxWidth,
	height = 56,
	flex = 'auto',
	classes,
	...rest
}: StyledTableCellProps): ReactElement => (
	<MuiTableCell
		component="div"
		sx={{
			flex,
			width,
			minWidth,
			maxWidth,
			height,
			lineHeight: `${height}px`,
		}}
		classes={classes}
		{...rest}
	/>
))(
	({ theme }) => `
		overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    padding: 0;
    padding-left: ${theme.spacing(1)};
    padding-right: ${theme.spacing(1)};
    box-sizing: border-box;
    color: inherit;
    border-bottom: none;
	`,
);

export interface TableCellProps<ObjectType extends DefaultObjectType> extends StyledTableCellProps {
  field: keyof ObjectType;
  data?: ObjectType;
  render?: (params: RenderCellParams<ObjectType>) => ReactElement;
  formatter?: (params: RenderCellParams<ObjectType>) => CellValue;
  flex?: number;
}

export const TableCell = <ObjectType extends DefaultObjectType>({
	field,
	data,
	render,
	formatter,
	children,
	...rest
}: TableCellProps<ObjectType>): ReactElement => {
	if (children) return <StyledTableCell {...rest}>{children}</StyledTableCell>;
	let value: CellValue | ReactElement = data ? data[field] : '';

	if (render) {
		value = render({ field, value, data });
	} else if (formatter) {
		value = formatter({ field, value, data });
	}

	return <StyledTableCell {...rest}>{value}</StyledTableCell>;
};
