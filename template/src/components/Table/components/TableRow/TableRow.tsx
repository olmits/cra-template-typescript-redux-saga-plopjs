import React from 'react';
import {
	TableRow as MuiTableRow,
	TableRowProps as MuiTableRowProps,
} from '@mui/material';

// eslint-disable-next-line import/prefer-default-export
export const TableRow = (props: MuiTableRowProps) => (
	<MuiTableRow {...props} />
);
