import React from 'react';
import {
	TableBody as MuiTableBody,
	TableBodyProps as MuiTableBodyProps,
} from '@mui/material';

// eslint-disable-next-line import/prefer-default-export
export const TableBody = (props: MuiTableBodyProps) => (
	<MuiTableBody {...props} />
);
