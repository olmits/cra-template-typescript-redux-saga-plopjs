import React from 'react';
import {
	TableHead as MuiTableHead,
	TableHeadProps as MuiTableHeadProps,
} from '@mui/material';

// eslint-disable-next-line import/prefer-default-export
export const TableHead = (props: MuiTableHeadProps) => (
	<MuiTableHead {...props} />
);
