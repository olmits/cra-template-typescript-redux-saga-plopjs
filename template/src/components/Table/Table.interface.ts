import { TableProps as MuiTableProps } from '@mui/material';
import { ReactElement } from 'react';

export type CellValue = string | number | boolean;

export interface DefaultObjectType {
  [index: string]: CellValue;
  id: string;
}

type SortDirection = 'asc' | 'desc';

type SortModel<ObjectType extends DefaultObjectType> = { field: keyof ObjectType; sort: SortDirection };

export interface RenderCellParams<ObjectType extends DefaultObjectType> {
  field: keyof ObjectType;
  value: CellValue;
  data?: ObjectType;
}

interface DefaultColumnType<ObjectType extends DefaultObjectType> {
  /**
   * Set the width of the column.
   * @default 100
   */
  width?: number;
  /**
   * If set, it indicates that a column has fluid width. It overrides width value.
   */
  flex?: number;
  /**
   * Sets the maximum width of a column.
   */
  maxWidth?: number;
  /**
   * Sets the minimum width of a column.
   * @default 50
   */
  minWidth?: number;
  /**
   * If `true`, hide the column.
   * @default false
   */
  hide?: boolean;
  /**
   * Sets the index of the row, which current field will occupy
   * @description Determine if there're multiple rows required, and indicated which row the current field will occupy
   * @default 0
   */
  rowIndex?: number;
  /**
   * Allows to align the column values in cells.
   */
  align?: 'left' | 'right' | 'center';
  /**
   * Function that allows to apply a formatter before rendering its value.
   * @returns
   */
  valueFormatter?: (params: RenderCellParams<ObjectType>) => CellValue;
  /**
   * Function that allows to override the component rendered as cell for this column.
   * @returns {ReactElement} The element to be rendered.
   */
  renderCell?: (params: RenderCellParams<ObjectType>) => ReactElement;
}

export interface ColumnType<ObjectType extends DefaultObjectType> extends DefaultColumnType<ObjectType> {
  /**
   * The column identifier.
   */
  field: keyof ObjectType;
  /**
   * Cell config, that will be applied for a mobile view.
   */
  mobileConfig?: DefaultColumnType<ObjectType>;
  /**
   * Class name that will be added in cells for that column.
   */
  cellClassName?: string;
  /**
   * The title of the column rendered in the column header cell.
   */
  headerName: string;
  /**
   * Class name that will be added in the column header cell.
   */
  headerClassName?: string;
  /**
   * Allows to render a component in the column header cell.
   * @returns {ReactElement} The element to be rendered.
   */
  renderHeader?: (params: RenderCellParams<ObjectType>) => ReactElement;
  /**
   * Header cell element alignment.
   */
  headerAlign?: 'left' | 'right' | 'center';
  /**
   * If `true`, the column is sortable.
   * @default true
   */
  sortable?: boolean;
}

export interface TableProps<ObjectType extends DefaultObjectType> extends MuiTableProps {
  /**
   * Set of columns of type [[ColumnType]].
   */
  columns: ColumnType<ObjectType>[];
  /**
   * Set of data of an generic [[ObjectType]].
   */
  data: ObjectType[];
  /**
   * Set the total number of data records.
   */
  count?: number;
  /**
   * Set the current page.
   * @default 1
   */
  page?: number;
  /**
   * Set the number of rows in one page.
   * @default 10
   */
  pageSize?: number;
  /**
   * Callback fired when the current page has changed.
   * @param page Index of the page displayed on the Grid.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onPageChange?: (page: number) => void;
  /**
   * Set the height in pixel of the column headers in the grid.
   * @default 56
   */
  headerHeight?: number;
  /**
   * Set the height in pixel of a row in the grid.
   * @default 52
   */
  rowHeight?: number;
  /**
   * Callback fired when the sort model changes.
   */
  onSortModelChange?: (model: SortModel<ObjectType>) => void;
  /**
   * Current sort model
   */
  sortModel?: SortModel<ObjectType>;
}
